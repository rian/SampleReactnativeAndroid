import React, { Component, PropTypes } from 'react';
import { 
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet } from 'react-native';

export default class MyScene extends Component {
 
  render() {

    let uriImage = {uri: 'http://res.cloudinary.com/kerjadulu/image/upload/v1431435766/oortgzzbf0ozkr7ey8fa.png'};
    return (
      <View style={styles.container}>
        <Image source={uriImage} style={styles.image} />
        <Text>Hello RCS Bandung</Text>

        <TouchableOpacity onPress={this.onSecondPage.bind(this)} style={styles.touchable}>
          <Text>Tap me to Next Scene</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.sampleListviewListener.bind(this)} style={styles.touchable}>
          <Text>Tap me to SampleListView</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.sampleListAPIListener.bind(this)} style={styles.touchable}>
          <Text>Tap me to SampleListAPI</Text>
        </TouchableOpacity>
      </View>
    )
  } 
  onSecondPage () {
    var navigator = this.props.navigator;
    
        navigator.push({
            id: 'SecondScene',
        });
  }
  sampleListviewListener () {
    var navigator = this.props.navigator;
    
        navigator.push({
            id: 'SampleListView',
        });
  }
  sampleListAPIListener () {
    var navigator = this.props.navigator;
    
        navigator.push({
            id: 'SampleListAPI',
        });
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  image: {
    width: 220,
    height: 100,
  },
  touchable: {
    marginTop: 10,
    backgroundColor: '#cccccc',
    padding: 10,
    borderRadius: 10
  }

});
