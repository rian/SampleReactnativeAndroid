/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry, Navigator } from 'react-native';

import MyScene from './MyScene';
import SecondScene from './SecondScene';
import SampleListView from './SampleListView';
import SampleListAPI from './SampleListAPI';

class samplenav extends Component {
  render() {
    return (
      <Navigator
        initialRoute={{ id:'MyScenePage', title: 'My Initial Scene', index: 0 }}
        renderScene={this.renderScene.bind(this)}
        configureScene={(route) => {
        if (route.sceneConfig) {
          return route.sceneConfig;
        }
        return Navigator.SceneConfigs.VerticalDownSwipeJump;
      }}
      />
    )
  }
  renderScene ( route, navigator ) {
      var routeId = route.id;
      switch (routeId) {
      	case 'MyScenePage':
          return ( <MyScene navigator={navigator}/> );
      		break;
        case 'SecondScene':
          return ( <SecondScene navigator={navigator}/> );
          break;
        case 'SampleListView':
          return ( <SampleListView navigator={navigator}/> );
          break;
        case 'SampleListAPI':
          return ( <SampleListAPI navigator={navigator}/> );
          break;
      	default:
      		// statements_def
      		break;
      }
  }
}



AppRegistry.registerComponent('samplenav', () => samplenav);
